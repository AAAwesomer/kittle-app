﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation; 
using UnityEngine.XR.ARSubsystems; 


public class PlaceDragonObject : MonoBehaviour
{
    ARRaycastManager raycastManager;
    Camera arCamera;
    public GameObject explotionObject;
    public GameObject confetti;
    public GameObject dragon;
    public GameObject[] dragons;
    public int index = 0;

    void Awake()
        {
            foreach (GameObject c in dragons)
            {
                c.SetActive(false);
            }
            dragons[0].SetActive(true);
        }

    // Start is called before the first frame update
    void Start()
    {
        raycastManager = GameObject.FindObjectOfType<ARRaycastManager>();
        arCamera = GameObject.FindObjectOfType<Camera>();
       
      
    }

    // Update is called once per frame
    void LateUpdate()
    {
    var viewportCenter = new Vector2(0.5f, 0.5f);
    var screenCenter = arCamera.ViewportToScreenPoint(viewportCenter);
    UpdateIndicator(screenCenter);
    }

    private void UpdateIndicator(Vector2 screenPosition)
{
    var hits = new List<ARRaycastHit>();

    raycastManager.Raycast(screenPosition, hits, TrackableType.Planes);

    // If there is at least one hit position
    if (hits.Count > 0)
    {
        // Get the pose data
        var placementPose = hits[0].pose;

        var camForward = arCamera.transform.forward;

        // We want the object to be flat
        camForward.y = 0;

        // Scale the vector be have a size of 1
        camForward = camForward.normalized;

        // Rotate to face in front of the camera
        placementPose.rotation = Quaternion.LookRotation(camForward);

        transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);

        placementPose.rotation = Quaternion.LookRotation(camForward);

      // Move the quad slightly above the floor to avoid z-fighting
      var newPosition = placementPose.position;
      newPosition.y += 0.001f;

      transform.SetPositionAndRotation(newPosition, placementPose.rotation);

     if (Input.GetMouseButtonDown(0))
    {
        SwapDragon();  
         //StartCoroutine(runConfettiExplosion());
    }

      if (Input.GetMouseButtonUp(0))
    {
        SwitchAnimation();
         //StartCoroutine(runConfetti());
    }


    if (Input.acceleration.x > 0.2f)
    {
        StartCoroutine(runConfettiExplosion());
    }
    else if (Input.acceleration.x < -0.2f)
    {
        StartCoroutine(runConfetti());
    }


    }

}

  private void SwapDragon()
        {
            dragons[index].SetActive(false);
            index++;
            index %= dragons.Length;
            dragons[index].SetActive(true);
        
        }

        private void SwitchAnimation() {

             dragons[index].GetComponent<Animator>().SetInteger("animation", Random.Range(0,20));
        }

        private IEnumerator runConfettiExplosion() {

             if (confetti.active ) {
                yield return new WaitForSeconds(1.0f);

             } else if (explotionObject.active ) {
                 yield return new WaitForSeconds(5.0f);
                 explotionObject.SetActive(false);
                
             } else {
                yield return new WaitForSeconds(3);
                 explotionObject.SetActive(true);
                yield return new WaitForSeconds(3);
                explotionObject.SetActive(false);
             }

               
        }

         private IEnumerator runConfetti() {

            if (explotionObject.active ) {
                 yield return new WaitForSeconds(5.0f);
             }
             else if (confetti.active ) {
                 yield return new WaitForSeconds(3.0f);
                confetti.SetActive(false);
              
             } else  {
                yield return new WaitForSeconds(3.0f);
            confetti.SetActive(true);
            yield return new WaitForSeconds(5);
            confetti.SetActive(false);
             }
             
           
        }



}
