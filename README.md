# Kittle App

An interactive world of pets, built for children suffering from chronic illness, pain and/or anxiety.

## Development

### Version

The app is built with Unity version 2019.4.14.

### Run locally
To run the app locally, clone the repo <br/>
`git clone https://gitlab.com/AAAwesomer/kittle-app.git`<br/>
Then, install the correct version of Unity if needed and import the project to Unity.
