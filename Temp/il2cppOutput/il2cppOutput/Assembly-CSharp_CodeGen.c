﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void AnimatorX::Start()
extern void AnimatorX_Start_m5241A364A573674927FC6186CE738B41B22C1C0C (void);
// 0x00000002 System.Void AnimatorX::Update()
extern void AnimatorX_Update_m50A6838D32908911B957F6BA1E91C4268E461527 (void);
// 0x00000003 System.Void AnimatorX::.ctor()
extern void AnimatorX__ctor_m31DD2E4E038585345FED37AB677042F8DE3C21CE (void);
// 0x00000004 System.Void PlaceDragonObject::Awake()
extern void PlaceDragonObject_Awake_m5F5661EC9015016E735D7A4ADA11CB3BD0465866 (void);
// 0x00000005 System.Void PlaceDragonObject::Start()
extern void PlaceDragonObject_Start_m64827C2EA1313ED8908B2FCA779775D5EA7701D8 (void);
// 0x00000006 System.Void PlaceDragonObject::LateUpdate()
extern void PlaceDragonObject_LateUpdate_m0263381AC0BA8977FB369761554ACD4CF7AC603C (void);
// 0x00000007 System.Void PlaceDragonObject::UpdateIndicator(UnityEngine.Vector2)
extern void PlaceDragonObject_UpdateIndicator_m6C1015C4F26AED3F5877B784202EF6DC4840235F (void);
// 0x00000008 System.Void PlaceDragonObject::SwapDragon()
extern void PlaceDragonObject_SwapDragon_m483427F45671A87F76CF6472482FF2C9C99E5CD6 (void);
// 0x00000009 System.Void PlaceDragonObject::SwitchAnimation()
extern void PlaceDragonObject_SwitchAnimation_m4C721B8FBED377E091A5E19E9BBCE6EE4731E48E (void);
// 0x0000000A System.Collections.IEnumerator PlaceDragonObject::runConfettiExplosion()
extern void PlaceDragonObject_runConfettiExplosion_mE690999279666A4A75873C8EBA73E058FCFF0C97 (void);
// 0x0000000B System.Collections.IEnumerator PlaceDragonObject::runConfetti()
extern void PlaceDragonObject_runConfetti_mFC135B7B52EE6BCFE7455FF8D02DE9C3C8335F5D (void);
// 0x0000000C System.Void PlaceDragonObject::.ctor()
extern void PlaceDragonObject__ctor_mAE87353B5160C82245C8A0C412A9F34FE1B1E17B (void);
// 0x0000000D System.Void MouseOrbitImproved::Start()
extern void MouseOrbitImproved_Start_m8EE4C7E7CF491F01A06A74C94FCE8752B6C35B6C (void);
// 0x0000000E System.Void MouseOrbitImproved::LateUpdate()
extern void MouseOrbitImproved_LateUpdate_m3B6E870EB05D6BE90A1B25C0EE68CBA75C121318 (void);
// 0x0000000F System.Single MouseOrbitImproved::ClampAngle(System.Single,System.Single,System.Single)
extern void MouseOrbitImproved_ClampAngle_mA31E5211EACF720D93A38DF3A3689A685B928860 (void);
// 0x00000010 System.Void MouseOrbitImproved::.ctor()
extern void MouseOrbitImproved__ctor_m62C4307C6B205B14B45B8E077717B0720E62AA7A (void);
// 0x00000011 System.Void Swapper::Awake()
extern void Swapper_Awake_m3FF80ACBA46F44D8F188FE3966287DE4BE43FA87 (void);
// 0x00000012 System.Void Swapper::OnGUI()
extern void Swapper_OnGUI_m7A5CD42193157B212A16D913E06D7766231BA2AB (void);
// 0x00000013 System.Void Swapper::.ctor()
extern void Swapper__ctor_mCEC4E4FEA28E2FC5C3C44EEBB8F93485C7195408 (void);
// 0x00000014 System.Void Suriyun.AnimatorController::SwapVisibility(UnityEngine.GameObject)
extern void AnimatorController_SwapVisibility_m54F20901BF512884D6EAC6E55CEC6EEFC6940612 (void);
// 0x00000015 System.Void Suriyun.AnimatorController::SetFloat(System.String)
extern void AnimatorController_SetFloat_m723DEB0F0D6A7A604471210989F05223496AB46C (void);
// 0x00000016 System.Void Suriyun.AnimatorController::SetInt(System.String)
extern void AnimatorController_SetInt_m3C5D7FA484ECADAFCF3C07D627C655D53BB87870 (void);
// 0x00000017 System.Void Suriyun.AnimatorController::SetBool(System.String)
extern void AnimatorController_SetBool_m2B0B9D75B99227BA83677126BF1697905048B8D0 (void);
// 0x00000018 System.Void Suriyun.AnimatorController::SetTrigger(System.String)
extern void AnimatorController_SetTrigger_mF481268AEA175A469E84DC7E8171A931F63AF5C5 (void);
// 0x00000019 System.Void Suriyun.AnimatorController::.ctor()
extern void AnimatorController__ctor_mE1ED207AB7BCE3A5E74949CA3C3801E51748B924 (void);
// 0x0000001A System.Void PlaceDragonObject_<runConfettiExplosion>d__13::.ctor(System.Int32)
extern void U3CrunConfettiExplosionU3Ed__13__ctor_mA3DA948CE17558830A5B7BA6A7B12B8912D92AAB (void);
// 0x0000001B System.Void PlaceDragonObject_<runConfettiExplosion>d__13::System.IDisposable.Dispose()
extern void U3CrunConfettiExplosionU3Ed__13_System_IDisposable_Dispose_mCBF5F5627C625A182F4CF5FCDC9F5FF89E2E238A (void);
// 0x0000001C System.Boolean PlaceDragonObject_<runConfettiExplosion>d__13::MoveNext()
extern void U3CrunConfettiExplosionU3Ed__13_MoveNext_m6C48D3727727776820573FFB5C25BEA81B622FE3 (void);
// 0x0000001D System.Object PlaceDragonObject_<runConfettiExplosion>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CrunConfettiExplosionU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC71C38DAA30D569AA9BCD729324C5E99D242F62 (void);
// 0x0000001E System.Void PlaceDragonObject_<runConfettiExplosion>d__13::System.Collections.IEnumerator.Reset()
extern void U3CrunConfettiExplosionU3Ed__13_System_Collections_IEnumerator_Reset_m16BFB7722D3B72CE5555BCC50CE67C45D38EC85C (void);
// 0x0000001F System.Object PlaceDragonObject_<runConfettiExplosion>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CrunConfettiExplosionU3Ed__13_System_Collections_IEnumerator_get_Current_m790176494D76EC8AF965E3A37DBB30F640600044 (void);
// 0x00000020 System.Void PlaceDragonObject_<runConfetti>d__14::.ctor(System.Int32)
extern void U3CrunConfettiU3Ed__14__ctor_mFD9E53C26DC27ACB2C3E9CF8417D963617249000 (void);
// 0x00000021 System.Void PlaceDragonObject_<runConfetti>d__14::System.IDisposable.Dispose()
extern void U3CrunConfettiU3Ed__14_System_IDisposable_Dispose_mED6B66C7DD97C69DB648F9E0CECAFAD278913096 (void);
// 0x00000022 System.Boolean PlaceDragonObject_<runConfetti>d__14::MoveNext()
extern void U3CrunConfettiU3Ed__14_MoveNext_m2755BF6A808ED78DE5B0C12B11D75CE47E2F402E (void);
// 0x00000023 System.Object PlaceDragonObject_<runConfetti>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CrunConfettiU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA36C5CF9C157B94440F930FCBF175AAD8DA5B98A (void);
// 0x00000024 System.Void PlaceDragonObject_<runConfetti>d__14::System.Collections.IEnumerator.Reset()
extern void U3CrunConfettiU3Ed__14_System_Collections_IEnumerator_Reset_m716DD8B148AA93C3C378C1C89DC01353BDC2F3B1 (void);
// 0x00000025 System.Object PlaceDragonObject_<runConfetti>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CrunConfettiU3Ed__14_System_Collections_IEnumerator_get_Current_mBB83A6CD3F780F4C48B4E353BD35AD7783210219 (void);
static Il2CppMethodPointer s_methodPointers[37] = 
{
	AnimatorX_Start_m5241A364A573674927FC6186CE738B41B22C1C0C,
	AnimatorX_Update_m50A6838D32908911B957F6BA1E91C4268E461527,
	AnimatorX__ctor_m31DD2E4E038585345FED37AB677042F8DE3C21CE,
	PlaceDragonObject_Awake_m5F5661EC9015016E735D7A4ADA11CB3BD0465866,
	PlaceDragonObject_Start_m64827C2EA1313ED8908B2FCA779775D5EA7701D8,
	PlaceDragonObject_LateUpdate_m0263381AC0BA8977FB369761554ACD4CF7AC603C,
	PlaceDragonObject_UpdateIndicator_m6C1015C4F26AED3F5877B784202EF6DC4840235F,
	PlaceDragonObject_SwapDragon_m483427F45671A87F76CF6472482FF2C9C99E5CD6,
	PlaceDragonObject_SwitchAnimation_m4C721B8FBED377E091A5E19E9BBCE6EE4731E48E,
	PlaceDragonObject_runConfettiExplosion_mE690999279666A4A75873C8EBA73E058FCFF0C97,
	PlaceDragonObject_runConfetti_mFC135B7B52EE6BCFE7455FF8D02DE9C3C8335F5D,
	PlaceDragonObject__ctor_mAE87353B5160C82245C8A0C412A9F34FE1B1E17B,
	MouseOrbitImproved_Start_m8EE4C7E7CF491F01A06A74C94FCE8752B6C35B6C,
	MouseOrbitImproved_LateUpdate_m3B6E870EB05D6BE90A1B25C0EE68CBA75C121318,
	MouseOrbitImproved_ClampAngle_mA31E5211EACF720D93A38DF3A3689A685B928860,
	MouseOrbitImproved__ctor_m62C4307C6B205B14B45B8E077717B0720E62AA7A,
	Swapper_Awake_m3FF80ACBA46F44D8F188FE3966287DE4BE43FA87,
	Swapper_OnGUI_m7A5CD42193157B212A16D913E06D7766231BA2AB,
	Swapper__ctor_mCEC4E4FEA28E2FC5C3C44EEBB8F93485C7195408,
	AnimatorController_SwapVisibility_m54F20901BF512884D6EAC6E55CEC6EEFC6940612,
	AnimatorController_SetFloat_m723DEB0F0D6A7A604471210989F05223496AB46C,
	AnimatorController_SetInt_m3C5D7FA484ECADAFCF3C07D627C655D53BB87870,
	AnimatorController_SetBool_m2B0B9D75B99227BA83677126BF1697905048B8D0,
	AnimatorController_SetTrigger_mF481268AEA175A469E84DC7E8171A931F63AF5C5,
	AnimatorController__ctor_mE1ED207AB7BCE3A5E74949CA3C3801E51748B924,
	U3CrunConfettiExplosionU3Ed__13__ctor_mA3DA948CE17558830A5B7BA6A7B12B8912D92AAB,
	U3CrunConfettiExplosionU3Ed__13_System_IDisposable_Dispose_mCBF5F5627C625A182F4CF5FCDC9F5FF89E2E238A,
	U3CrunConfettiExplosionU3Ed__13_MoveNext_m6C48D3727727776820573FFB5C25BEA81B622FE3,
	U3CrunConfettiExplosionU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC71C38DAA30D569AA9BCD729324C5E99D242F62,
	U3CrunConfettiExplosionU3Ed__13_System_Collections_IEnumerator_Reset_m16BFB7722D3B72CE5555BCC50CE67C45D38EC85C,
	U3CrunConfettiExplosionU3Ed__13_System_Collections_IEnumerator_get_Current_m790176494D76EC8AF965E3A37DBB30F640600044,
	U3CrunConfettiU3Ed__14__ctor_mFD9E53C26DC27ACB2C3E9CF8417D963617249000,
	U3CrunConfettiU3Ed__14_System_IDisposable_Dispose_mED6B66C7DD97C69DB648F9E0CECAFAD278913096,
	U3CrunConfettiU3Ed__14_MoveNext_m2755BF6A808ED78DE5B0C12B11D75CE47E2F402E,
	U3CrunConfettiU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA36C5CF9C157B94440F930FCBF175AAD8DA5B98A,
	U3CrunConfettiU3Ed__14_System_Collections_IEnumerator_Reset_m716DD8B148AA93C3C378C1C89DC01353BDC2F3B1,
	U3CrunConfettiU3Ed__14_System_Collections_IEnumerator_get_Current_mBB83A6CD3F780F4C48B4E353BD35AD7783210219,
};
static const int32_t s_InvokerIndices[37] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	1120,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	1077,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	37,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
